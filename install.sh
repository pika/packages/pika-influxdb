#! /bin/bash

docker network ls | grep pika-net > /dev/null || docker network create --driver bridge pika-net
docker compose up -d --build
if [ $? -ne 0 ]; then
  echo "Error: Cannot install pika-influxdb."
  exit 1
fi

echo "Starting InfluxDB..."
sleep 10
echo ""
echo "Setup passwords for PIKA database:"
echo ""

docker exec -it pika-influxdb bash /pika-setup/setup_influxdb.sh
