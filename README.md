### 1. Install InfluxDB

```sh
git clone https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-influxdb.git
```

The following script installs the container for InfluxDB. It also sets up the pika database.
```
./install.sh
```

**Hints:**

Add InfluxDB http endpoint to firewall policy, e.g. with
```
firewall-cmd --zone=public --permanent --add-port=8086/tcp
systemctl restart firewalld
```

All performance data older than 28 days will be deleted. Feel free to change the [retention policy](https://docs.influxdata.com/influxdb/v1/query_language/manage-database/#modify-retention-policies-with-alter-retention-policy) according to your needs using the InfluxDB shell (influx).


Our setup on ZIH uses a short-term and long-term database.
This setup enables us to carry out analyses over a longer period of time. At the moment this solution cannot be published yet, therefore the same InfluxDB credentials for `influxdb-st` and `influxdb-lt` should be used for the configuration in `pika-server`.
