#!/bin/bash

admin_password_request ()
{
  echo "Please enter a password for the user 'admin' and confirm it with [ENTER]."
  unset PWD;
  while IFS= read -r -s -n1 pass; do
    [[ -z $pass ]] && { printf '\n'; break; } 
    if [[ $pass == $'\x7f' ]]; then
      [[ -n $PWD ]] && PWD=${PWD%?}
      printf '\b \b'
    else
      printf '*'
      PWD+=$pass
    fi
  done
  ADMINPW=$PWD
}

read_password_request ()
{
  echo "Please enter a password for the user 'readonly' and confirm it with [ENTER]."
  unset PWD;
  while IFS= read -r -s -n1 pass; do
    [[ -z $pass ]] && { printf '\n'; break; } 
    if [[ $pass == $'\x7f' ]]; then
      [[ -n $PWD ]] && PWD=${PWD%?}
      printf '\b \b'
    else
      printf '*'
      PWD+=$pass
    fi
  done
  READPW=$PWD
}

error_check()
{
  if [ $1 -ne 0 ]; then
    echo "Creating the pika database failed."
    exit 1
  fi
}

admin_password_request
read_password_request

commands="CREATE USER admin WITH PASSWORD '${ADMINPW}' WITH ALL PRIVILEGES"
echo "${commands}" | influx
error_check $?

commands="CREATE DATABASE pika"
echo "${commands}" | influx -username admin -password ${ADMINPW}
error_check $?

commands="CREATE RETENTION POLICY shortterm ON pika DURATION 28d REPLICATION 1 SHARD DURATION 7d DEFAULT"
echo "${commands}" | influx -database pika -username admin -password ${ADMINPW}
error_check $?

commands="CREATE USER readonly WITH PASSWORD '${READPW}'"
echo "${commands}" | influx -database pika -username admin -password ${ADMINPW}
error_check $?

commands="GRANT READ ON pika TO readonly"
echo "${commands}" | influx -database pika -username admin -password ${ADMINPW}
error_check $?


echo "Pika database successfully created."
exit 0

