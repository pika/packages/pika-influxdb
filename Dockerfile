FROM influxdb:1.7.9

RUN mkdir /pika-setup
COPY ./setup /pika-setup
WORKDIR /pika-setup